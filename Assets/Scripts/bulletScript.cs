﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletScript : MonoBehaviour
{
    //Reference to ammo of bullets
    public SimpleObjectPool ammo;

    /// <summary>
    /// this takes a bullet from the ammo, and shoots it from the aim
    /// </summary>
    /// <param name="aim">place where the bullet will release</param>
    /// <param name="ammo">pool of bullets</param>
    public void shoot(Transform aim, SimpleObjectPool ammo)
    {
        this.ammo = ammo;
        gameObject.transform.SetParent(null);
        transform.position = aim.position;
        transform.rotation = aim.rotation;
        GetComponent<Rigidbody>().velocity = aim.transform.forward * 10;
        StartCoroutine(waitToReturn());
    }

    /// <summary>
    /// this returns the bullet itself to the ammo of the enemy after 2 seconds of being released
    /// </summary>
    /// <returns></returns>
    IEnumerator waitToReturn()
    {
        yield return new WaitForSeconds(2);
        ammo.ReturnObject(gameObject);
    }
}