﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameControllerScript : MonoBehaviour
{
    //Counter of enemies destroyed (this never resets)
    private int enemiesDestroyedCount = 0;

    //Counters of enemies destroyed (this can be reset)
    private int enemiesDestroyed = 0;
    private int enemiesDestroyedForLife = 0;

    //Counter of damage received to the player
    private int damageReceived = 0;

    /// <summary>
    /// We add value to the counters of enemies destroyed
    /// </summary>
    /// <param name="amount">Value</param>
    public void addEnemyDestroyed(int amount)
    {
        enemiesDestroyed = enemiesDestroyed + amount;
        enemiesDestroyedCount = enemiesDestroyedCount + amount;
        enemiesDestroyedForLife = enemiesDestroyedForLife + amount;
    }

    /// <summary>
    /// We add value to the counter of damage received 
    /// </summary>
    /// <param name="amount">Value</param>
    public void addDamageReceived(int amount)
    {
        damageReceived = damageReceived + amount;
    }

    /// <summary>
    /// Returns the value of the counter enemiesDestroyed
    /// </summary>
    /// <returns>Value of enemies destroyed</returns>
    public int getEnemiesDestroyed()
    {
        return enemiesDestroyed;
    }

    /// <summary>
    /// Returns the value of the counter enemiesDestroyedCount
    /// </summary>
    /// <returns>Value of enemies destroyed</returns>
    public int getEnemiesDestroyedCount()
    {
        return enemiesDestroyedCount;
    }

    /// <summary>
    /// Returns the value of the counter enemiesDestroyedForLife
    /// </summary>
    /// <returns>Value of enemies destroyed</returns>
    public int getEnemiesDestroyedForLife()
    {
        return enemiesDestroyedForLife;
    }

    /// <summary>
    /// Returns the value of the counter damageReceived
    /// </summary>
    /// <returns>Value of damage received</returns>
    public int getDamageReceived()
    {
        return damageReceived;
    }

    /// <summary>
    /// Resets the enemiesDestroyed counter to zero
    /// </summary>
    public void resetEnemies()
    {
        enemiesDestroyed = 0;
    }

    /// <summary>
    /// Resets the enemiesDestroyedForLife and damageReceived counters to zero
    /// </summary>
    public void resetDamage()
    {
        enemiesDestroyedForLife = 0;
        damageReceived = 0;
    }
}
