﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class barrierScript : MonoBehaviour
{
    //Indicates the damage of the barrier
    private int damage = 0;

    // Update is called once per frame
    void Update()
    {
        //If damage it's 3 or more, we destroy the barrier
        if(damage >= 3)
        {
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// When the barrier triggers with something
    /// </summary>
    /// <param name="other">The object that is triggering with the barrier</param>
    private void OnTriggerEnter(Collider other)
    {
        //if there's a trigger with a bullet of the enemy, we add damage to the wall
        //and we return the bullet to the ammo of the enemy
        if (other.gameObject.tag == "bulletEnemy")
        {
            damage++;
            other.GetComponent<bulletScript>().ammo.ReturnObject(other.gameObject);
        }
    }
}