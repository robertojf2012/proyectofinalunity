﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using Random = UnityEngine.Random;

public class enemyScript : MonoBehaviour
{
    //Reference to ammo of bullets
    public SimpleObjectPool ammo;

    //Reference to the ammo of the player
    public SimpleObjectPool ammoPlayer;

    //Reference to the pool of enemies
    public SimpleObjectPool enemyPool;

    //Reference to the aim of the gun
    public Transform aim;

    //Reference to the game controller
    public gameControllerScript GAME;

    //Indicates the speed the enemy is gonna move
    private float speed = 0;

    /// <summary>
    /// This generates a new enemy in an specific position.
    /// We configure the speed, and we start the coroutine to start shooting
    /// </summary>
    /// <param name="position">Position where the enemy it's gonna be</param>
    /// <param name="enemyPool">The pool of enemies where this will be taken</param>
    /// <param name="GAME">The game controller</param>
    /// <param name="ammoPlayer">Ammo of the player</param>
    public void generateMe(Vector3 position, SimpleObjectPool enemyPool, gameControllerScript GAME, SimpleObjectPool ammoPlayer, Transform parent)
    {
        //We establish references
        this.enemyPool = enemyPool;
        this.GAME = GAME;
        this.ammoPlayer = ammoPlayer;

        //Set the parent of the enemy
        transform.SetParent(parent);
        
        //Set the volicity to 0 and the angular velocity
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        
        //We set a random speed to the enemy
        speed = Random.Range(2, 4);
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, speed);

        //We set the position where the enemy it's gonna be
        transform.localPosition = new Vector3(position.x, position.y, position.z);

        //We start the coroutine of shooting
        StartCoroutine(waitToShootAgain());
    }

    /// <summary>
    /// When the enemy collides with something
    /// </summary>
    /// <param name="collision">The object that is colliding with the enemy</param>
    private void OnCollisionEnter(Collision collision)
    {
        //if there's a collision with a wall, we reverse the speed of the enemy
        if (collision.gameObject.tag == "wall")
        {
            speed = speed * -1;
            GetComponent<Rigidbody>().velocity = new Vector3(0, 0, speed);
        }
    }

    /// <summary>
    /// When the enemy triggers with something
    /// </summary>
    /// <param name="other">The object that is triggering with the enemy</param>
    private void OnTriggerEnter(Collider other)
    {
        //if there's a trigger with a bullet of the player, we add one kill to the counter of enemies destroyed
        //then we return the bullet to the ammo of the player and the enemy itself to the pool of enemies
        if (other.gameObject.tag == "bulletPlayer")
        {
            GAME.addEnemyDestroyed(1);
            ammoPlayer.ReturnObject(other.gameObject);
            enemyPool.ReturnObject(gameObject);
        }
    }

    /// <summary>
    /// Gets or creates a bullet to be shoot from the aim
    /// </summary>
    public void shoot()
    {
        GameObject newBullet = ammo.GetObject();
        newBullet.GetComponent<bulletScript>().shoot(aim, ammo);
    }

    /// <summary>
    /// this makes the enemy shoot a bullet randomly, from (0.5 seconds to 2 seconds)
    /// </summary>
    /// <returns></returns>
    public IEnumerator waitToShootAgain()
    {
        float seconds = Random.Range(0.5f,2);
        yield return new WaitForSeconds(seconds);
        shoot();
        StartCoroutine(waitToShootAgain());
    }
}
