﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using Random = UnityEngine.Random;

public class barrierGeneratorScript : MonoBehaviour
{
    //Reference to the prefab of the barrier
    public GameObject barrier;
    
    //Reference to imageTarget
    public Transform ImageTarget;
    
    // Start is called before the first frame update
    void Start()
    {
        generateBarriers();
    }
    
    /// <summary>
    /// This generates 5 barriers in rows
    /// </summary>
    public void generateBarriers()
    {
        generateBarrier(-0.48f);
        generateBarrier(-1);
        generateBarrier(-1.52f);
        generateBarrier(-2.04f);
        generateBarrier(-2.56f);
    }

    /// <summary>
    /// This generates a barrier in an specific position in 'x' and 'y', and random position in 'z'
    /// </summary>
    /// <param name="x"></param>
    private void generateBarrier(float x)
    {
        GameObject barrera = Instantiate(barrier, new Vector3(x, 0.719f, Random.Range(1.50f, -1.50f)), Quaternion.identity);
        barrera.transform.SetParent(ImageTarget);
    }
}