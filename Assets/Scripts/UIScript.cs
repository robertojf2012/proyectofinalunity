﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;// Required when using Event data.

public class UIScript : MonoBehaviour
{
    //References to the UI texts
    public Text textEnemies;
    public Text textDamage;

    //Reference to the game controller
    public gameControllerScript GAME;

    //Reference to the player
    public GameObject player;

    //This two indicates if the up or down buttons are being pressed    
    private static bool upKeyPressed;
    private static bool downKeyPressed;

    // Update is called once per frame
    void Update()
    {
        //Condition to write the values of damage received and enemies destroyed, only when textDamage and textEnemies are not null
        if (textDamage != null && textEnemies != null)
        {
            textDamage.text = "Damage: " + GAME.getDamageReceived().ToString();
            textEnemies.text = "Enemies destroyed: " + GAME.getEnemiesDestroyedCount().ToString();
        }

        //Condition if up button is being pressed
        if (upKeyPressed)
        {
            //We move the player only if it's alive
            if (player.GetComponent<playerScript>().alive == true)
            {
                player.GetComponent<Rigidbody>().MovePosition(player.GetComponent<Transform>().transform.position + Vector3.forward * Time.deltaTime * 4);
            }
        }

        //Condition if down button is being pressed
        if (downKeyPressed)
        {
            //We move the player only if it's alive
            if (player.GetComponent<playerScript>().alive == true)
            {
                player.GetComponent<Rigidbody>().MovePosition(player.GetComponent<Transform>().transform.position + Vector3.back * Time.deltaTime * 4);
            }
        }
    }

    /// <summary>
    /// Makes the player shoot a bullet when the shoot UI button is clicked
    /// </summary>
    public void shootButton()
    {
        player.GetComponent<playerScript>().shoot();
    }

    /// <summary>
    /// This sets the upKeyPressed value to true when up button it's being pressed
    /// </summary>
    public void upArrowDown()
    {
        upKeyPressed = true;
    }

    /// <summary>
    /// This sets the upKeyPressed value to false when up button it's being released
    /// </summary>
    public void upArrowUp()
    {
        upKeyPressed = false;
    }

    /// <summary>
    /// This sets the downKeyPressed value to true when down button it's being pressed
    /// </summary>
    public void downArrowDown()
    {
        downKeyPressed = true;
    }

    /// <summary>
    /// This sets the downKeyPressed value to false when down button it's being released
    /// </summary>
    public void downArrowUp()
    {
        downKeyPressed = false;
    }
}
