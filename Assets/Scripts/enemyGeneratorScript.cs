﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyGeneratorScript : MonoBehaviour
{
    //Reference to the enemies pool
    public SimpleObjectPool enemyPool;

    //Reference to the ammo of the player
    public SimpleObjectPool ammoPlayer;

    //Reference to the game controller
    public gameControllerScript GAME;
    
    //Reference to the parent of the enemy
    public Transform parent;
    
    // Start is called before the first frame update
    void Start()
    {
        generateEnemies();
    }

    /// <summary>
    /// This generates an enemy in an specific position in 'x' and 'y', and random position in 'z'
    /// We take an enemy from the pool, then we call the generateMe function from the enemy itself
    /// </summary>
    /// <param name="x">Position in x</param>
    public void generateEnemy(float x)
    {
        Vector3 position = new Vector3(x, parent.localPosition.y, Random.Range(1,-1));
        GameObject enemy = enemyPool.GetObject();
        enemy.GetComponent<Rigidbody>().velocity = Vector3.zero;
        enemy.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        enemy.transform.SetParent(parent);
        enemy.GetComponent<enemyScript>().generateMe(position, enemyPool, GAME, ammoPlayer, parent);
    }

    /// <summary>
    /// This generates 5 enemies in rows
    /// </summary>
    private void generateEnemies()
    {
        generateEnemy(1.36f);
        generateEnemy(2.36f);
        generateEnemy(3.36f);
        generateEnemy(4.36f);
        generateEnemy(5.36f);
    }

    // Update is called once per frame
    void Update()
    {
        //if there's no enemies on the game, we reset the counter of enemies destroyed and we regenerate 5 more
        if (GAME.getEnemiesDestroyed() == 5)
        {
            GAME.resetEnemies();
            generateEnemies();
        }

        if (Input.GetKeyDown(KeyCode.K))
        {
            generateEnemies();
        }
    }
}