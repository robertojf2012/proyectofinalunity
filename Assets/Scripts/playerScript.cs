﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerScript : MonoBehaviour
{
    //Reference to ammo of bullets
    public SimpleObjectPool ammo;

    //Reference to the aim of the gun
    public Transform aim;

    //Reference to the game controller
    public gameControllerScript GAME;

    //Reference to the barrier generator
    public barrierGeneratorScript BARRIER;

    //Indicates if the player is alive
    public bool alive = true;

    //Reference to the weapon of the player
    public Rigidbody weapon;
    
    //Reference to parent ImafeTarget
    public Transform ImageTarget;
    private void Start()
    {
        transform.SetParent(ImageTarget);
    }

    // Update is called once per frame
    void Update()
    {
        //Condition for the player to die, only when he receives 10 or more shoots from the enemies
        if (GAME.getDamageReceived() >= 10)
        {
            GAME.resetDamage();
            alive = false;
            
            //this makes the player throw the weapon to the floor
            weapon.useGravity = true;
            weapon.constraints = RigidbodyConstraints.None;
        }

        //if the player kills 10 enemies.. the damage it's reset to zero and we regerate the barriers
        if (GAME.getEnemiesDestroyedForLife() == 10)
        {
            GAME.resetDamage();
            BARRIER.generateBarriers();
        }

        //Keys for controlling the player (move and shoot) only when he's alive
        if (Input.GetKey(KeyCode.UpArrow) && alive)
        {
            GetComponent<Rigidbody>().MovePosition(transform.position + Vector3.forward * Time.deltaTime * 4);
        }

        if (Input.GetKey(KeyCode.DownArrow) && alive)
        {
            GetComponent<Rigidbody>().MovePosition(transform.position + Vector3.back * Time.deltaTime * 4);
        }

        if (Input.GetKeyDown(KeyCode.Space) && alive)
        {
            shoot();
        }
    }

    /// <summary>
    /// When the player collides with something
    /// </summary>
    /// <param name="collision">The object that is colliding with the player</param>
    private void OnCollisionEnter(Collision collision)
    {
        //if there's a collision with a wall, we make stop the player from moving
        if (collision.gameObject.tag == "wall")
        {
            gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        }
    }

    /// <summary>
    /// When the player triggers with something
    /// </summary>
    /// <param name="other">The object that is triggering with the player</param>
    private void OnTriggerEnter(Collider other)
    {
        //if there's a trigger with a bullet from an enemy, we add damage to the player 
        //and we return the bullet to the ammo of the enemy
        if (other.gameObject.tag == "bulletEnemy")
        {
            GAME.addDamageReceived(1);
            other.GetComponent<bulletScript>().ammo.ReturnObject(other.gameObject);
        }
    }

    /// <summary>
    /// Gets or creates a bullet to be shoot from the aim
    /// </summary>
    public void shoot()
    {
       GameObject newBullet = ammo.GetObject();
       newBullet.GetComponent<bulletScript>().shoot(aim, ammo);
    }
}
